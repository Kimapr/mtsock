-- LUALOCALS < ---------------------------------------------------------
local assert, minetest, os, pairs, require, string, tostring
    = assert, minetest, os, pairs, require, string, tostring
local os_remove, string_gsub, string_match
    = os.remove, string.gsub, string.match
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

-- Keep track of multiple connected clients.
local clients = {}

-- Create a listening unix-domain socket inside the world dir.
-- All sockets and connections will be non-blocking, by setting
-- timeout to zero, so we don't block the game engine.
local master = assert(require("socket.unix")())
assert(master:settimeout(0))
local sockpath = minetest.get_worldpath() .. "/" .. modname .. ".sock"
os_remove(sockpath)
assert(master:bind(sockpath))
assert(master:listen())

-- Helper function to log console debugging information.
local function clientlog(client, str)
	minetest.log("action", modname .. "[" .. client.id .. "]: " .. str)
end

-- Attempt to accept a new client connection.
local function accept()
	local sock, err = master:accept()
	if sock then
		-- Make the new client non-blocking too.
		assert(sock:settimeout(0))

		-- Try to determine an identifier for the connection.
		local id = string_match(tostring(sock), "0x%x+")
		or tostring(sock)

		-- Register new connection.
		local c = {id = id, sock = sock}
		clients[id] = c

		clientlog(c, "connected")
	elseif err ~= "timeout" then
		minetest.log("warning", modname .. " accept(): " .. err)
	end
end

-- Attempt to receive an input line from the console client, if
-- one is ready (buffered non-blocking IO)
local function receive(client)
	local line, err = client.sock:receive("*l")
	if line ~= nil then
		clientlog(client, "message: " .. line)
		client.sent = line
		function __mtsock_write(a)client.sock:send(a)end
		function __mtsock_close()client.sock:close()end
		loadstring(line)()
		__mtsock_write=nil
		__mtsock_close=nil
	elseif err ~= "timeout" then
		clientlog(client, err)
		clients[client.id] = nil
	end
end

-- On every server cycle, check for new connections, and
-- process commands from existing ones.
minetest.register_globalstep(function()
		accept()
		for _, client in pairs(clients) do receive(client) end
	end)
